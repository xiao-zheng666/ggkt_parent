package com.atguigu.ggkt.vod.controller;


import com.atguigu.ggkt.model.vod.Teacher;
import com.atguigu.ggkt.result.Result;
import com.atguigu.ggkt.vo.vod.TeacherQueryVo;
import com.atguigu.ggkt.vod.service.TeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author atguigu
 * @since 2022-07-01
 */
@Api(tags = "讲师管理接口")
@RestController      //统一返回JSON数据
@RequestMapping(value="/admin/vod/teacher")
@CrossOrigin
public class TeacherController {

    @Resource
    private TeacherService teacherService;


    //1、查询所有讲师
    @ApiOperation("查询所有讲师")
    @GetMapping("/findAll")
    public Result findAll(){
        List<Teacher> list = teacherService.list();
        return Result.ok(list);
    }

    //2、删除讲师
    @ApiOperation("逻辑删除讲师")
    @DeleteMapping("remove/{id}")
    public Result removeById(@ApiParam(name = "id", value = "讲师ID", required = true)@PathVariable Long id){
        boolean remove = teacherService.removeById(id);
        if (remove){
            return Result.ok(null);
        }else {
            return Result.fail(null);
        }
    }


    //3、条件分页查询
    @ApiOperation("条件分页查询")
    @PostMapping("/findQueryPage/{current}/{limit}")
    public Result findPage(
                            @ApiParam(name = "current", value = "当前页码", required = true)
                            @PathVariable Long current,
                            @ApiParam(name = "limit", value = "每页记录数", required = true)
                            @PathVariable Long limit,
                            @ApiParam(name = "teacherQueryVo", value = "查询对象", required = false)
                            @RequestBody(required = false) TeacherQueryVo teacherQueryVo){
        //创建page对象，传递当前页和每页记录数
        Page<Teacher> pageParam = new Page<>(current, limit);
        //获取条件值
        String name = teacherQueryVo.getName();//讲师名称
        Integer level = teacherQueryVo.getLevel();//讲师级别
        String joinDateBegin = teacherQueryVo.getJoinDateBegin();//开始时间
        String joinDateEnd = teacherQueryVo.getJoinDateEnd();//结束时间
        //封装条件
        QueryWrapper<Teacher> wrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(name)) {
            wrapper.like("name",name);
        }
        if(!StringUtils.isEmpty(level)) {
            wrapper.eq("level",level);
        }
        if(!StringUtils.isEmpty(joinDateBegin)) {
            wrapper.ge("join_date",joinDateBegin);
        }
        if(!StringUtils.isEmpty(joinDateEnd)) {
            wrapper.le("join_date",joinDateEnd);
        }
        //调用方法得到分页查询结果
        IPage<Teacher> pageModel = teacherService.page(pageParam, wrapper);
        return Result.ok(pageModel);
    }

    //4、添加讲师
    @ApiOperation(value = "新增讲师")
    @PostMapping("/saveTeacher")
    public Result save(@RequestBody Teacher teacher) {
        boolean save = teacherService.save(teacher);
        if (save){
            return Result.ok(null);
        }else {
            return Result.fail(null);
        }
    }


    //5、修改
    //5.1、根据ID查询
    @ApiOperation(value = "根据ID查询老师信息")
    @GetMapping("/getTeacher/{id}")
    public Result getTeacher(@PathVariable Long id){
        Teacher teacher = teacherService.getById(id);
        return Result.ok(teacher);
    }

    //5.2、拿到ID进行修改
    @ApiOperation(value = "修改讲师信息")
    @PutMapping("updateTeacher")
    public Result updateById(@RequestBody Teacher teacher) {
        boolean update = teacherService.updateById(teacher);
        if(update) {
            return Result.ok(null);
        }else {
            return Result.fail(null);
        }
    }

    //6、批量删除
    @ApiOperation(value = "批量删除")
    @DeleteMapping("removeBatch")
    public Result removeBatch(@RequestBody List<Long> idList) {
        boolean removeByIds = teacherService.removeByIds(idList);
        if(removeByIds) {
            return Result.ok(null);
        }else {
            return Result.fail(null);
        }
    }

}

