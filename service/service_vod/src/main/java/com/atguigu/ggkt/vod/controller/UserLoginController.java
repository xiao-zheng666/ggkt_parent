package com.atguigu.ggkt.vod.controller;

import com.atguigu.ggkt.result.Result;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

//http://localhost:9528/dev-api/vue-admin-template/user/login
@CrossOrigin
@RestController
@RequestMapping("/admin/vod/user")
public class UserLoginController {

    //用户登录
    @PostMapping("/login")
    public Result login(){
//      {code: 20000, data: {token: "admin-token"}}
        Map<String,Object> map = new HashMap<>();
        map.put("token","admin-token");
        return Result.ok(map).code(20000);
    }

    //info
    @GetMapping("/info")
    public Result info(){
        Map<String,Object> map = new HashMap<>();
//        avatar: "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif"
//        introduction: "I am a super administrator"
//        name: "Super Admin"
//        roles: ["admin"]
        map.put("roles","admin");
        map.put("introduction","I am a super administrator");
        map.put("name","Super Admin");
        map.put("avatar","https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        return Result.ok(map).code(20000);
    }



    //退出登录
    @PostMapping("/logout")
    public Result logout(){
        Map<String,Object> map = new HashMap<>();

        return Result.ok(null).code(20000);
    }
}
